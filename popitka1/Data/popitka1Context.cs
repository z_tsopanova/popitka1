﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace popitka1.Models
{
    public class popitka1Context : DbContext
    {
        public popitka1Context (DbContextOptions<popitka1Context> options)
            : base(options)
        {
        }

        public DbSet<popitka1.Models.proekt> proekt { get; set; }
    }
}
