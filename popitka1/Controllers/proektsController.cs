﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using popitka1.Models;

namespace popitka1.Controllers
{
    [Produces("application/json")]
    [Route("api/proekts")]
    public class proektsController : Controller
    {
        private readonly popitka1Context _context;

        public proektsController(popitka1Context context)
        {
            _context = context;
        }

        // GET: api/proekts
        [HttpGet]
        public IEnumerable<proekt> Getproekt()
        {
            return _context.proekt;
        }

        // GET: api/proekts/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Getproekt([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var proekt = await _context.proekt.SingleOrDefaultAsync(m => m.ID == id);

            if (proekt == null)
            {
                return NotFound();
            }

            return Ok(proekt);
        }

        // PUT: api/proekts/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Putproekt([FromRoute] int id, [FromBody] proekt proekt)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != proekt.ID)
            {
                return BadRequest();
            }

            _context.Entry(proekt).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!proektExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/proekts
        [HttpPost]
        public async Task<IActionResult> Postproekt([FromBody] proekt proekt)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.proekt.Add(proekt);
            await _context.SaveChangesAsync();

            return CreatedAtAction("Getproekt", new { id = proekt.ID }, proekt);
        }

        // DELETE: api/proekts/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Deleteproekt([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var proekt = await _context.proekt.SingleOrDefaultAsync(m => m.ID == id);
            if (proekt == null)
            {
                return NotFound();
            }

            _context.proekt.Remove(proekt);
            await _context.SaveChangesAsync();

            return Ok(proekt);
        }

        private bool proektExists(int id)
        {
            return _context.proekt.Any(e => e.ID == id);
        }
    }
}